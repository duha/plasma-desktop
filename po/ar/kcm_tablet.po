# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2022, 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2024-01-17 18:31+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.1\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "الأساسي (مبدئي)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "بالطّول"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "بالعرض"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "طولي معكوس"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "عرضي معكوس"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "اتبع الشاشة النشطة"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "‏%1 - (‏%2،‏%3 ‏%4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "وائم الخرج"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "وائم الخرج في اللوحي"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "حجم مخصّص"

#: ui/main.qml:31
#, kde-format
msgid "No drawing tablets found."
msgstr "لم يعثر على أي لوح رسم"

#: ui/main.qml:39
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "الجهاز:"

#: ui/main.qml:77
#, kde-format
msgid "Target display:"
msgstr "العرض الهدف:"

#: ui/main.qml:96
#, kde-format
msgid "Orientation:"
msgstr "الاتّجاه:"

#: ui/main.qml:108
#, kde-format
msgid "Left-handed mode:"
msgstr "وضع الأعسر:"

#: ui/main.qml:118
#, kde-format
msgid "Area:"
msgstr "المنطقة:"

#: ui/main.qml:206
#, kde-format
msgid "Resize the tablet area"
msgstr "غير الحجم مساحة اللوحي"

#: ui/main.qml:230
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "اقفل النّسبة الباعيّة"

#: ui/main.qml:238
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "‏%1،%2 - %3×%4"

#: ui/main.qml:247
#, kde-format
msgid "Pen Button 1"
msgstr "زر القلم 1"

#: ui/main.qml:248
#, kde-format
msgid "Pen Button 2"
msgstr "زر القلم 2"

#: ui/main.qml:249
#, kde-format
msgid "Pen Button 3"
msgstr "زر القلم 3"

#: ui/main.qml:293
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "اللوح:"

#: ui/main.qml:304
#, kde-format
msgid "None"
msgstr "لا شيء"

#: ui/main.qml:326
#, kde-format
msgid "Button %1:"
msgstr "الزر %1:"

#~ msgid "Tool Button 1"
#~ msgstr "زر الأدوات 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zayed.alsaidi@gmail.com"

#~ msgid "Tablet"
#~ msgstr "لوحي"

#~ msgid "Configure drawing tablets"
#~ msgstr "اضبط لوحي الرسم"
