# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-24 00:38+0000\n"
"PO-Revision-Date: 2020-10-25 18:49+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Üldine"

#: package/contents/ui/code/tools.js:32
#, kde-format
msgid "Remove from Favorites"
msgstr "Eemalda lemmikute seast"

#: package/contents/ui/code/tools.js:36
#, kde-format
msgid "Add to Favorites"
msgstr "Lisa lemmikute sekka"

#: package/contents/ui/code/tools.js:60
#, kde-format
msgid "On All Activities"
msgstr "Kõigis tegevustes"

#: package/contents/ui/code/tools.js:110
#, kde-format
msgid "On the Current Activity"
msgstr "Aktiivses tegevuses"

#: package/contents/ui/code/tools.js:124
#, kde-format
msgid "Show in Favorites"
msgstr "Näita lemmikutes"

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Icon:"
msgstr "Ikoon:"

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:85
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Open icon chooser dialog"
#| msgid "Choose..."
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Vali..."

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:97
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:110
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:150
#, fuzzy, kde-format
#| msgid "General:"
msgctxt "General options"
msgid "General:"
msgstr "Üldine:"

#: package/contents/ui/ConfigGeneral.qml:151
#, fuzzy, kde-format
#| msgid "Sort alphabetically"
msgid "Always sort applications alphabetically"
msgstr "Sordi tähestiku järgi"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgid "Use compact list item style"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:171
#, fuzzy, kde-format
#| msgid "Configure enabled search plugins"
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Seadista lubatud otsingupluginaid"

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgid "Sidebar position:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Right"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Left"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:198
#, fuzzy, kde-format
#| msgid "Show in Favorites"
msgid "Show favorites:"
msgstr "Näita lemmikutes"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:215
#, fuzzy, kde-format
#| msgid "Show applications by name"
msgid "Show other applications:"
msgstr "Rakenduste näitamine nime järgi"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:224
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Show buttons for:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:237
#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Power"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:246
#, kde-format
msgid "Session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:255
#, kde-format
msgid "Power and session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:264
#, kde-format
msgid "Show action button captions"
msgstr ""

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Rakendused"

#: package/contents/ui/Footer.qml:113
#, kde-format
msgid "Places"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:153
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr ""

#: package/contents/ui/Header.qml:80
#, kde-format
msgid "Open user settings"
msgstr ""

#: package/contents/ui/Header.qml:259
#, kde-format
msgid "Keep Open"
msgstr ""

#: package/contents/ui/KickoffGridView.qml:92
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr ""

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Leave"
msgstr "Väljumine"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "More"
msgstr ""

#: package/contents/ui/main.qml:311
#, fuzzy, kde-format
#| msgid "Edit Applications..."
msgid "Edit Applications…"
msgstr "Muuda rakendusi..."

#: package/contents/ui/PlacesPage.qml:48
#, fuzzy, kde-format
#| msgid "Computer"
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Arvuti"

#: package/contents/ui/PlacesPage.qml:49
#, fuzzy, kde-format
#| msgid "History"
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Ajalugu"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr ""

#~ msgctxt "@item:inmenu Reset icon to default"
#~ msgid "Clear Icon"
#~ msgstr "Puhasta ikoon"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Otsi ..."

#, fuzzy
#~| msgid "Leave"
#~ msgid "Leave…"
#~ msgstr "Väljumine"

#, fuzzy
#~| msgid "Edit Applications..."
#~ msgid "Updating applications…"
#~ msgstr "Muuda rakendusi..."

#~ msgid "%2@%3 (%1)"
#~ msgstr "%2@%3 (%1)"

#~ msgid "%1@%2"
#~ msgstr "%1@%2"

#, fuzzy
#~| msgid "Leave"
#~ msgid "Leave..."
#~ msgstr "Väljumine"

#, fuzzy
#~| msgctxt "@item:inmenu Open icon chooser dialog"
#~| msgid "Choose..."
#~ msgid "More..."
#~ msgstr "Vali..."

#~ msgid "Applications updated."
#~ msgstr "Rakendused on uuendatud."

#~ msgid "Switch tabs on hover"
#~ msgstr "Kaartide vahetamine hiirekursori kaardile viimisel"

#~ msgid "All Applications"
#~ msgstr "Kõik rakendused"

#~ msgid "Favorites"
#~ msgstr "Lemmikud"

#~ msgid "Often Used"
#~ msgstr "Sageli kasutatud"

#~ msgid "Active Tabs"
#~ msgstr "Aktiivsed kaardid"

#~ msgid "Inactive Tabs"
#~ msgstr "Mitteaktiivsed kaardid"

#~ msgid ""
#~ "Drag tabs between the boxes to show/hide them, or reorder the visible "
#~ "tabs by dragging."
#~ msgstr ""
#~ "Kaarte kastide vahel lohistades saab neid näidata lasta või peita, samuti "
#~ "saab lohistades muuta nähtavate kaartide järjestust."

#~ msgid "Expand search to bookmarks, files and emails"
#~ msgstr "Otsingu laiendamine järjehoidjatele, failidele ja kirjadele"

#~ msgid "Appearance"
#~ msgstr "Välimus"
