# SPDX-FileCopyrightText: 2021, 2022, 2024 Xavier Besnard <xavier.besnard@kde.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2024-01-22 22:10+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Primaire (Par défaut)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Portrait"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Paysage"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Portrait inversé"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Paysage inversé"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Suivre l'écran actif"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2, %3 %4 × %5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Faire correspondre à la sortie"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Faire correspondre à la sortie pour une tablette"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Taille personnalisée"

#: ui/main.qml:31
#, kde-format
msgid "No drawing tablets found."
msgstr "Aucune tablette de dessin n'a été trouvée."

#: ui/main.qml:39
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Périphérique :"

#: ui/main.qml:77
#, kde-format
msgid "Target display:"
msgstr "Affichage cible :"

#: ui/main.qml:96
#, kde-format
msgid "Orientation:"
msgstr "Orientation :"

#: ui/main.qml:108
#, kde-format
msgid "Left-handed mode:"
msgstr "Mode gaucher :"

#: ui/main.qml:118
#, kde-format
msgid "Area:"
msgstr "Zone :"

#: ui/main.qml:206
#, kde-format
msgid "Resize the tablet area"
msgstr "Redimensionner la zone de tablette"

#: ui/main.qml:230
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Verrouiller le ration d'affichage"

#: ui/main.qml:238
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1, %2 - %3 × %4"

#: ui/main.qml:247
#, kde-format
msgid "Pen Button 1"
msgstr "Bouton du stylet 1"

#: ui/main.qml:248
#, kde-format
msgid "Pen Button 2"
msgstr "Bouton du stylet 2"

#: ui/main.qml:249
#, kde-format
msgid "Pen Button 3"
msgstr "Bouton du stylet 3"

#: ui/main.qml:293
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Pavé :"

#: ui/main.qml:304
#, kde-format
msgid "None"
msgstr "Aucun"

#: ui/main.qml:326
#, kde-format
msgid "Button %1:"
msgstr "Bouton %1 :"

#~ msgid "Tool Button 1"
#~ msgstr "Bouton 1 d'outils"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Xavier Besnard"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xavier.besnard@neuf.fr"

#~ msgid "Tablet"
#~ msgstr "Tablette"

#~ msgid "Configure drawing tablets"
#~ msgstr "Configurer les tablettes de dessin"
