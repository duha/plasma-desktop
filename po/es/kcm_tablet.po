# Spanish translations for kcm_tablet.po package.
# Copyright (C) 2021 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Automatically generated, 2021.
# SPDX-FileCopyrightText: 2021, 2022, 2024 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: kcm_tablet\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2024-01-14 23:59+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Primaria (predeterminada)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Retrato"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Paisaje"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Retrato invertido"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Paisaje invertido"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Seguir la pantalla activa"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Ajustar a la salida"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Ajustar la salida en la tableta"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Tamaño personalizado"

#: ui/main.qml:31
#, kde-format
msgid "No drawing tablets found."
msgstr "No se han encontrado tabletas de dibujo."

#: ui/main.qml:39
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Dispositivo:"

#: ui/main.qml:77
#, kde-format
msgid "Target display:"
msgstr "Pantalla de destino:"

#: ui/main.qml:96
#, kde-format
msgid "Orientation:"
msgstr "Orientación:"

#: ui/main.qml:108
#, kde-format
msgid "Left-handed mode:"
msgstr "Modo para zurdos:"

#: ui/main.qml:118
#, kde-format
msgid "Area:"
msgstr "Área:"

#: ui/main.qml:206
#, kde-format
msgid "Resize the tablet area"
msgstr "Cambiar el tamaño del área de la tableta"

#: ui/main.qml:230
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Bloquear las proporciones"

#: ui/main.qml:238
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:247
#, kde-format
msgid "Pen Button 1"
msgstr "Botón 1 del lápiz"

#: ui/main.qml:248
#, kde-format
msgid "Pen Button 2"
msgstr "Botón 2 del lápiz"

#: ui/main.qml:249
#, kde-format
msgid "Pen Button 3"
msgstr "Botón 3 del lápiz"

#: ui/main.qml:293
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Panel táctil:"

#: ui/main.qml:304
#, kde-format
msgid "None"
msgstr "Ninguno"

#: ui/main.qml:326
#, kde-format
msgid "Button %1:"
msgstr "Botón %1:"

#~ msgid "Tool Button 1"
#~ msgstr "Botón de herramienta 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Eloy Cuadra"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ecuadra@eloihr.net"

#~ msgid "Tablet"
#~ msgstr "Tableta"

#~ msgid "Configure drawing tablets"
#~ msgstr "Configurar las tabletas de dibujo"
